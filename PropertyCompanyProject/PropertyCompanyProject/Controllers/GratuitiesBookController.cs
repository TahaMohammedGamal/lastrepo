﻿using System.Threading.Tasks;
using System.Web.Mvc;
using PropertyCompanyProject.DAL;
using PropertyCompanyProject.HelperClasses;
using PropertyCompanyProject.Models;

namespace PropertyCompanyProject.Controllers
{
    public class GratuitiesBookController : Controller
    {
        // GET: GratuitiesBook
        public ActionResult GratuitiesBook_Page()
        {
            InClientBag();
            return View();
        }
        private void InClientBag()
        {
            ClientBag bagObj = new ClientBag();

            bagObj += new ServerParam("OperObject", new GratuitiesBook());
            var bag = Helper.ConvertDataToJson(bagObj);

            ViewBag.ServerParams = bag;
        }

        public async Task<string> SaveData(GratuitiesBook GratBook)
        {
            var res = await GratBookDAL.AddNewOperatingGradBookAsync(GratBook);

            return Helper.ConvertDataToJson(res);
        }
    }
}