﻿using PropertyCompanyProject.Models;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PropertyCompanyProject.Controllers
{
    public class AccountController : Controller
    {
        PropertyCompanyEntities db = new PropertyCompanyEntities();

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string username, string password)
        {

            bool userValid = db.AspNetUsers.Any(user => user.UserName == username && user.PasswordHash == password);

            if (userValid)
            {
                HttpCookie auth = new HttpCookie("auth", Request.Form["username"] + "|" + Request.Form["password"]);
                auth.Expires.AddDays(30);
                Response.Cookies.Add(auth);

                return RedirectToAction("Index", "Home");
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public ActionResult Logout()
        {
            Response.Cookies["auth"].Expires = DateTime.Now.AddDays(-1);

            return RedirectToAction("Login", "Account");
        }
    }
}