﻿using System.Collections.Generic;
using System.Web.Mvc;
using PropertyCompanyProject.DAL;
using PropertyCompanyProject.Models;
using System.Threading.Tasks;
using PropertyCompanyProject.HelperClasses;

namespace PropertyCompanyProject.Controllers
{
    public class TowerSandController : Controller
    {
        // GET: TowerSand
        public ActionResult AddTowerSand()
        {
            InClientBag();
            return View();
        }
        private void InClientBag()
        {
            ClientBag bag = new ClientBag();

            List<object> TowerDetails = TowerDetailsDAL.GetTowerBasicInfo();
            List<object> ContractorDetails = TowerDetailsDAL.GetContractorBasicInfo();

            bag += new ServerParam("Sand", new TowerSandCost());
            bag += new ServerParam("Towers", TowerDetails);
            bag += new ServerParam("Contractors", ContractorDetails);

            var bagObj = Helper.ConvertDataToJson(bag);

            ViewBag.ServerParams = bagObj;

        }

        public async Task<string> SaveData(TowerSandCost JSand)
        {
            var res = await TowerSandDAL.AddTowerSandCostAsync(JSand);

            return Helper.ConvertDataToJson(res);
        }
    }
}