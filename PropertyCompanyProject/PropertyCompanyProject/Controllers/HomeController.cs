﻿using System.IO;
using System.Linq;
using System.Web.Mvc;
using PropertyCompanyProject.HelperClasses;

namespace PropertyCompanyProject.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Images =
                Directory.GetFiles(Server.MapPath(Constants.SliderImagesFolderPath))
                    .Select(fileName => $"{Constants.SliderImagesFolderPath}/{Path.GetFileName(fileName)}");
            return View();
        }
    }
}