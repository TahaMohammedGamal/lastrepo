﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Threading.Tasks;
using PropertyCompanyProject.DAL;
using PropertyCompanyProject.HelperClasses;
using PropertyCompanyProject.Models;

namespace PropertyCompanyProject.Controllers
{
    public class TowerElectricityController : Controller
    {
        // GET: TowerElectricity
        public ActionResult AddTowerElectricity()
        {
            InClientBag();
            return View();
        }
        private void InClientBag()
        {
            ClientBag bag = new ClientBag();

            List<object> TowerDetails = TowerDetailsDAL.GetTowerBasicInfo();
            List<object> ContractorDetails = TowerDetailsDAL.GetContractorBasicInfo();

            bag += new ServerParam("Electricity", new TowerElectricityCost());
            bag += new ServerParam("Towers", TowerDetails);
            bag += new ServerParam("Contractors", ContractorDetails);

            var bagObj = Helper.ConvertDataToJson(bag);

            ViewBag.ServerParams = bagObj;

        }

        public async Task<string> SaveData(TowerElectricityCost JTower)
        {
            var res = await TowerElectricityDAL.AddTowerElectricityCostAsync(JTower);

            return Helper.ConvertDataToJson(res);
        }
    }
}