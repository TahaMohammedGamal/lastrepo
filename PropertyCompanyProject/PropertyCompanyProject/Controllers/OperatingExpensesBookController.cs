﻿using System.Threading.Tasks;
using System.Web.Mvc;
using PropertyCompanyProject.DAL;
using PropertyCompanyProject.HelperClasses;
using PropertyCompanyProject.Models;

namespace PropertyCompanyProject.Controllers
{
    public class OperatingExpensesBookController : Controller
    {
        // GET: OperatingExpensesBook
        public ActionResult OperatingExpensesBook()
        {
            InClientBag();
            return View();
        }

        private void InClientBag()
        {
            ClientBag bagObj = new ClientBag();

            bagObj += new ServerParam("OperObject", new ExpensesBook());
            var bag = Helper.ConvertDataToJson(bagObj);

            ViewBag.ServerParams = bag;
        }

        public async Task<string> SaveData(ExpensesBook JExpenses)
        {
            var res = await OperatingExeDAL.AddNewOperatingExeBookAsync(JExpenses);

            return Helper.ConvertDataToJson(res);
        }
    }
}