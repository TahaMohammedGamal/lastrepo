﻿using System.Threading.Tasks;
using System.Web.Mvc;
using PropertyCompanyProject.HelperClasses;
using PropertyCompanyProject.Models;
using static PropertyCompanyProject.HelperClasses.Helper;
using static PropertyCompanyProject.DAL.CommissionsBookDAL;

namespace PropertyCompanyProject.Controllers
{
    public class CommissionsBookController : Controller
    {
        public ActionResult CommissionsBookPage()
        {
            InClientBag();
            return View();
        }

        private void InClientBag()
        {
            var clientBag = new ClientBag();
            clientBag += new ServerParam("Commission", new CommissionsBook());
            ViewBag.Commission = ConvertDataToJson(clientBag);
        }

        public async Task<string> SaveData(CommissionsBook commissionsBook)
        {
            return ConvertDataToJson(await AddNewCommissionsBookAsync(commissionsBook));
        }
    }
}