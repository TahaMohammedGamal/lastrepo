﻿using System.Threading.Tasks;
using System.Web.Mvc;
using PropertyCompanyProject.DAL;
using PropertyCompanyProject.HelperClasses;
using PropertyCompanyProject.Models;

namespace PropertyCompanyProject.Controllers
{
    public class ZakatBookController : Controller
    {
        // GET: ZakatBook
        public ActionResult ZakatBook_Page()
        {
            InClientBag();
            return View();
        }

        private void InClientBag()
        {
            ClientBag bagObj = new ClientBag();

            bagObj += new ServerParam("OperObject", new ZakatBook());
            var bag = Helper.ConvertDataToJson(bagObj);

            ViewBag.ServerParams = bag;
        }
        public async Task<string> SaveData(ZakatBook zakatbook)
        {
            var res = await ZakatBookDAL.AddNewZakatBookAsync(zakatbook);
            return Helper.ConvertDataToJson(res);
        }
    }
}