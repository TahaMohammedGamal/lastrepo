﻿using PropertyCompanyProject.DAL;
using PropertyCompanyProject.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using PropertyCompanyProject.HelperClasses;

namespace PropertyCompanyProject.Controllers
{
    public class TowerIronController : Controller
    {
        // GET: TowerIron
        public ActionResult AddTowerIron()
        {
            InClientBag();

            return View();
        }
        private void InClientBag()
        {
            ClientBag bag = new ClientBag();

            List<object> TowerDetails = TowerDetailsDAL.GetTowerBasicInfo();
            List<object> ContractorDetails = TowerDetailsDAL.GetContractorBasicInfo();

            bag += new ServerParam("Iron", new TowerIronCost());
            bag += new ServerParam("Towers", TowerDetails);
            bag += new ServerParam("Contractors", ContractorDetails);

            var bagObj = Helper.ConvertDataToJson(bag);

            ViewBag.ServerParams = bagObj;

        }

        public async Task<string> SaveData(TowerIronCost JTower)
        {
            var res = await TowerIronDAL.AddTowerIronCostAsync(JTower);

            return Helper.ConvertDataToJson(res);
        }
    }
}