﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using PropertyCompanyProject.DAL;
using PropertyCompanyProject.HelperClasses;
using PropertyCompanyProject.Models;

namespace PropertyCompanyProject.Controllers
{
    public class TowerCementController : Controller
    {
        // GET: TowerDetails
        public ActionResult AddCementDetails()
        {
            InClientBag();
            return View();
        }

        private void InClientBag()
        {
            ClientBag bag = new ClientBag();

            List<object> TowerDetails = TowerDetailsDAL.GetTowerBasicInfo();
            List<object> ContractorDetails = TowerDetailsDAL.GetContractorBasicInfo();

            bag += new ServerParam("Cement", new TowerCementCost());
            bag += new ServerParam("Towers", TowerDetails);
            bag += new ServerParam("Contractors", ContractorDetails);

            var bagObj = Helper.ConvertDataToJson(bag);

            ViewBag.ServerParams = bagObj;

        }

        public async Task<string> SaveData(TowerCementCost JTower)
        {
            var res = await TowerCementDAL.AddTowerCementCostAsync(JTower);

            return Helper.ConvertDataToJson(res);
        }
    }
}