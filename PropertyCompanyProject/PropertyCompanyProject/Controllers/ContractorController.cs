﻿using System.Web.Mvc;
using PropertyCompanyProject.DAL;
using PropertyCompanyProject.HelperClasses;
using PropertyCompanyProject.Models;

namespace PropertyCompanyProject.Controllers
{
    public class ContractorController : Controller
    {
        // GET: Contractor
        public ActionResult Index()
        {
            InClientBag();
            return View();
        }

        private void InClientBag()
        {
            ClientBag bagObj = new ClientBag();

            bagObj += new ServerParam("ContractorObject", new Contractor());
            var bag = Helper.ConvertDataToJson(bagObj);

            ViewBag.ServerParams = bag;
        }

        public  string SaveData(Contractor JContractor)
        {
            var res = ContractorDAL.AddContractor(JContractor);

            return Helper.ConvertDataToJson(res);
        }
    }
}