﻿using System.Threading.Tasks;
using System.Web.Mvc;
using PropertyCompanyProject.DAL;
using PropertyCompanyProject.HelperClasses;
using PropertyCompanyProject.Models;

namespace PropertyCompanyProject.Controllers
{
    public class PersonalWithdrawalsBookController : Controller
    {
        // GET: PersonalWithdrawalsBook
        public ActionResult PersonalWithdrawals_Page()
        {
            InClientBag();
            return View();
        }

        private void InClientBag()
        {
            ClientBag bagObj = new ClientBag();

            bagObj += new ServerParam("OperObject", new PersonalWithdrawalsBook());
            var bag = Helper.ConvertDataToJson(bagObj);

            ViewBag.ServerParams = bag;
        }
        public async Task<string> SaveData(PersonalWithdrawalsBook personalwithdrawalsbook)
        {
            var res = await PersonalWithdrawalsBookDAL.AddNewPersonalWithdrawalsBookAsync(personalwithdrawalsbook);
            return Helper.ConvertDataToJson(res);
        }
    }
}