﻿using System.Collections.Generic;
using System.Web.Mvc;
using PropertyCompanyProject.DAL;
using PropertyCompanyProject.Models;
using System.Threading.Tasks;
using PropertyCompanyProject.HelperClasses;

namespace PropertyCompanyProject.Controllers
{
    public class TowerWorkerFeesController : Controller
    {
        // GET: TowerWorkerFees
        public ActionResult AddTowerWorkerFees()
        {
            InClientBag();
            return View();
        }
        private void InClientBag()
        {
            ClientBag bag = new ClientBag();

            List<object> TowerDetails = TowerDetailsDAL.GetTowerBasicInfo();
            List<object> ContractorDetails = TowerDetailsDAL.GetContractorBasicInfo();

            bag += new ServerParam("WorkerFees", new TowerWorkerFee());
            bag += new ServerParam("Towers", TowerDetails);
            bag += new ServerParam("Contractors", ContractorDetails);

            var bagObj = Helper.ConvertDataToJson(bag);

            ViewBag.ServerParams = bagObj;

        }

        public async Task<string> SaveData(TowerWorkerFee JTower)
        {
            var res = await TowerWorkerFeesDAL.AddTowerWorkerFeeAsync(JTower);

            return Helper.ConvertDataToJson(res);
        }
    }
}