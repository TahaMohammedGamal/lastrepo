﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PropertyCompanyProject.HelperClasses;
using PropertyCompanyProject.Models;

namespace PropertyCompanyProject.Controllers
{
    public class LandBooksController : Controller
    {
        private PropertyCompanyEntities db = new PropertyCompanyEntities();

        // GET: LandBooks
        public ActionResult Index()
        {
            var landBooks = db.LandBooks.Include(l => l.LandBuyer);
            ViewBag.landbooks = Helper.ConvertDataToJson(landBooks.ToList());
            return View(landBooks.ToList());
        }

        // GET: LandBooks/Create
        public ActionResult Create()
        {
            //ViewBag.BillID = new SelectList(db.LandBuyers, "BillID", "SellerName");
            ViewBag.LandBooks = Helper.ConvertDataToJson(db.LandBooks.Include(ll => ll.LandBuyer).ToList());
            return View();
        }

        // POST: LandBooks/Create
        [HttpPost]
        public ActionResult Create(int BillID, DateTime Date, string SellerName, string Status, decimal TotalPrice, decimal PaidPrice, decimal ReminderPrice, string Description)
        {
            //[Bind(Include = "BillID,Date,SellerName,Status,TotalPrice,PaidPrice,ReminderPrice,Description")] LandBook landBook

            LandBook landBook = new LandBook();
            LandBuyer LandBuyer = new LandBuyer();
            landBook.BillID = BillID;
            landBook.Date = Convert.ToDateTime(Date);
            landBook.ID = 0;
            landBook.PaidPrice = PaidPrice;
            landBook.ReminderPrice = ReminderPrice;

            LandBuyer.BillID = BillID;
            LandBuyer.Description = Description;
            LandBuyer.Status = Status;
            LandBuyer.SellerName = SellerName;
            LandBuyer.TotalPrice = TotalPrice;
            landBook.LandBuyer = LandBuyer;

            if (ModelState.IsValid)
            {
                db.LandBooks.Add(landBook);
                db.SaveChanges();
            }

            return RedirectToAction("Index");
            //ViewBag.BillID = new SelectList(db.LandBuyers, "BillID", "SellerName", landBook.BillID);
            //return View(landBook);
        }
        
        // GET: LandBooks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LandBook landBook = db.LandBooks.Find(id);
            if (landBook == null)
            {
                return HttpNotFound();
            }
            else
            {
                db.LandBooks.Remove(landBook);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }

    }
}
