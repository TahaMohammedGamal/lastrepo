﻿using PropertyCompanyProject.DAL;
using PropertyCompanyProject.Models;
using System.Threading.Tasks;
using System.Web.Mvc;
using PropertyCompanyProject.HelperClasses;

namespace PropertyCompanyProject.Controllers
{
    public class SalariesBookController : Controller
    {
        // GET: SalariesBook
        public ActionResult AddEmployeeSalary()
        {
            InClientBag();
            return View();
        }
        private void InClientBag()
        {
            ClientBag bagObj = new ClientBag();
            bagObj += new ServerParam("OperObject", new SalariesBook());
            var bag = Helper.ConvertDataToJson(bagObj);
            ViewBag.ServerParams = bag;
        }
        public async Task<string> SaveData(SalariesBook salariesbook)
        {
            var res = await SalariesBookDAL.AddNewSalariesBookAsync(salariesbook);
            return Helper.ConvertDataToJson(res);
        }
    }
}