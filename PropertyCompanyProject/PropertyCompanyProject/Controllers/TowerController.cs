﻿using System.Threading.Tasks;
using System.Web.Mvc;
using PropertyCompanyProject.DAL;
using PropertyCompanyProject.HelperClasses;
using PropertyCompanyProject.Models;

namespace PropertyCompanyProject.Controllers
{
    public class TowerController : Controller
    {
        // GET: Tower
        public ActionResult Index()
        {
            InClientBag();
            return View();
        }

        private void InClientBag()
        {
            ClientBag bagObj = new ClientBag();

            bagObj += new ServerParam("TowerObject", new TowerBook());
            var bag = Helper.ConvertDataToJson(bagObj);

            ViewBag.ServerParams = bag;
        }


        public async Task<string> SaveData(TowerBook JTower)
        {
            var res = await TowerDAL.AddTowerDetailsAsync(JTower);

            return Helper.ConvertDataToJson(res);
        }
    }
}