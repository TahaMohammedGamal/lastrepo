﻿using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static PropertyCompanyProject.HelperClasses.Constants;

namespace PropertyCompanyProject.Controllers
{
    public class HomePageImagesController : Controller
    {
        #region Actions

        public ActionResult HomePageImages()
        {
            ViewBag.Images =
                new DirectoryInfo(Server.MapPath(SliderImagesFolderPath)).GetFiles()
                    .OrderByDescending(file => file.CreationTime)
                    .ToArray()
                    .Select(file => $"{SliderImagesFolderPath}/{Path.GetFileName(file.Name)}");
            return View();
        }

        [HttpPost]
        public ActionResult UploadNewImage(HttpPostedFileBase uploadedImageFile)
        {
            if (uploadedImageFile != null && uploadedImageFile.ContentLength > 0 &&
                IsValidImageExtension(uploadedImageFile.FileName))
                uploadedImageFile.SaveAs(Path.Combine(Server.MapPath(SliderImagesFolderPath),
                    uploadedImageFile.FileName));
            return RedirectToAction("HomePageImages");
        }

        public ActionResult DeleteImage(string imageToBeDeleted)
        {
            System.IO.File.Delete(Server.MapPath(imageToBeDeleted));
            return RedirectToAction("HomePageImages");
        }

        #endregion

        #region Methods

        private bool IsValidImageExtension(string imageFileName)
        {
            return Path.GetExtension(imageFileName)?.ToLower() == ".jpg" ||
                   Path.GetExtension(imageFileName)?.ToLower() == ".png" ||
                   Path.GetExtension(imageFileName)?.ToLower() == ".jpeg" ||
                   Path.GetExtension(imageFileName)?.ToLower() == ".gif";
        }

        #endregion
    }
}