﻿using PropertyCompanyProject.DAL;
using PropertyCompanyProject.HelperClasses;
using PropertyCompanyProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PropertyCompanyProject.Controllers
{
    public class EmployeesBookController : Controller
    {
        // GET: EmployeesBook
        public ActionResult AddNewEmployee()
        {
            InClientBag();
            return View();
        }

        private void InClientBag()
        {
            ClientBag bagObj = new ClientBag();
            bagObj += new ServerParam("EmployeeObject", new Employee());
            var bag = Helper.ConvertDataToJson(bagObj);
            ViewBag.ServerParams = bag;
        }

        public string SaveData(Employee emp)
        {
            var res = EmployeeBookDAL.AddNewEmployee(emp);
            return Helper.ConvertDataToJson(res);
        }

    }
}