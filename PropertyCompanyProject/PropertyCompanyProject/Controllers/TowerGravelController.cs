﻿using System.Collections.Generic;
using System.Web.Mvc;
using PropertyCompanyProject.DAL;
using PropertyCompanyProject.Models;
using System.Threading.Tasks;
using PropertyCompanyProject.HelperClasses;

namespace PropertyCompanyProject.Controllers
{
    public class TowerGravelController : Controller
    {
        // GET: TowerGravel
        public ActionResult AddTowerGravel()
        {
            InClientBag();
            return View();
        }
        private void InClientBag()
        {
            ClientBag bag = new ClientBag();

            List<object> TowerDetails = TowerDetailsDAL.GetTowerBasicInfo();
            List<object> ContractorDetails = TowerDetailsDAL.GetContractorBasicInfo();

            bag += new ServerParam("Gravel", new TowerGravelCost());
            bag += new ServerParam("Towers", TowerDetails);
            bag += new ServerParam("Contractors", ContractorDetails);

            var bagObj = Helper.ConvertDataToJson(bag);

            ViewBag.ServerParams = bagObj;

        }

        public async Task<string> SaveData(TowerGravelCost JGravel)
        {
            var res = await TowerGravelDAL.AddTowerGravelCostAsync(JGravel);

            return Helper.ConvertDataToJson(res);
        }
    }
}