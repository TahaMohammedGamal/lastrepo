//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PropertyCompanyProject.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class FlatBuyer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FlatBuyer()
        {
            this.SellingFlatBooks = new HashSet<SellingFlatBook>();
        }
    
        public int ID { get; set; }
        public string BuyerName { get; set; }
        public string Description { get; set; }
        public decimal Area { get; set; }
        public string Floor { get; set; }
        public string Services { get; set; }
        public bool Status { get; set; }
        public decimal TotalPrice { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SellingFlatBook> SellingFlatBooks { get; set; }
    }
}
