﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PropertyCompanyProject.Models;

namespace PropertyCompanyProject.DAL
{
    public class TowerDetailsDAL
    {
        #region BasicInfo
        public static List<object> GetTowerBasicInfo()
        {
            try
            {
                using (PropertyCompanyEntities dbcontext = new PropertyCompanyEntities())
                {
                    List<object> dbTower = dbcontext.TowerBooks
                        .Select(obj=>new { ID = obj.TowerID , Name = obj.TowerName})
                        .ToList<object>();

                    return dbTower;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Task<List<object>> GetTowerBasicInfoAsync()
        {
            return Task.Run(() => GetTowerBasicInfo());
        }


        public static List<object> GetContractorBasicInfo()
        {
            try
            {
                using (PropertyCompanyEntities dbcontext = new PropertyCompanyEntities())
                {
                    List<object> dbContractor = dbcontext.Contractors
                                        .Select(obj => new { ID = obj.ContractorID, Name = obj.Name })
                                        .ToList<object>();

                    return dbContractor;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Task<List<object>> GetContractorBasicInfoAsync()
        {
            return Task.Run(() => GetContractorBasicInfo());
        }
        #endregion
    }
}