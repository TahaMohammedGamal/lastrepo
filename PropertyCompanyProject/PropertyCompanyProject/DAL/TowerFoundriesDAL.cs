﻿using System;
using System.Threading.Tasks;
using PropertyCompanyProject.Models;

namespace PropertyCompanyProject.DAL
{
    public class TowerFoundriesDAL
    {
        public static int AddTowerFoundriesCost(TowerFoundriesCost nwFoundries)
        {
            try
            {
                using (PropertyCompanyEntities dbcontext = new PropertyCompanyEntities())
                {
                    dbcontext.TowerFoundriesCosts.Add(nwFoundries);
                    if (dbcontext.SaveChanges() > 0)
                        return 1;


                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static Task<int> AddTowerFoundriesCostAsync(TowerFoundriesCost nwBook)
        {
            return Task.Run(() => AddTowerFoundriesCost(nwBook));
        }
    }
}