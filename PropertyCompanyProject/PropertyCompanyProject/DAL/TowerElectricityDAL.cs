﻿using System;
using System.Threading.Tasks;
using PropertyCompanyProject.Models;

namespace PropertyCompanyProject.DAL
{
    public class TowerElectricityDAL
    {

        public static int AddTowerElectricityCost(TowerElectricityCost nwElectricity)
        {
            try
            {
                using (PropertyCompanyEntities dbcontext = new PropertyCompanyEntities())
                {
                    dbcontext.TowerElectricityCosts.Add(nwElectricity);
                    if (dbcontext.SaveChanges() > 0)
                        return 1;


                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static Task<int> AddTowerElectricityCostAsync(TowerElectricityCost nwBook)
        {
            return Task.Run(() => AddTowerElectricityCost(nwBook));
        }
    }
}