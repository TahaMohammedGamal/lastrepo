﻿using System;
using System.Threading.Tasks;
using PropertyCompanyProject.Models;

namespace PropertyCompanyProject.DAL
{
    public class TowerDAL
    {
        public static int AddTowerDetails(TowerBook nwTower)
        {
            try
            {
                using (PropertyCompanyEntities dbcontext = new PropertyCompanyEntities())
                {
                    dbcontext.TowerBooks.Add(nwTower);
                    if (dbcontext.SaveChanges() > 0)
                        return 1;
                    
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static Task<int> AddTowerDetailsAsync(TowerBook nwTower)
        {
            return Task.Run(() => AddTowerDetails(nwTower));
        }

    }
}