﻿using System;
using System.Threading.Tasks;
using PropertyCompanyProject.Models;

namespace PropertyCompanyProject.DAL
{
    public class TowerSandDAL
    {
        public static int AddTowerSandCost(TowerSandCost nwSand)
        {
            try
            {
                using (PropertyCompanyEntities dbcontext = new PropertyCompanyEntities())
                {
                    dbcontext.TowerSandCosts.Add(nwSand);
                    if (dbcontext.SaveChanges() > 0)
                        return 1;


                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static Task<int> AddTowerSandCostAsync(TowerSandCost nwBook)
        {
            nwBook.TotalCost = Convert.ToDecimal(nwBook.Quantity) * nwBook.UnitCost;
            return Task.Run(() => AddTowerSandCost(nwBook));
        }
    }
}