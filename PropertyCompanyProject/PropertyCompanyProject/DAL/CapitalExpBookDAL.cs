﻿using System;
using System.Threading.Tasks;
using PropertyCompanyProject.Models;

namespace PropertyCompanyProject.DAL
{
    public class CapitalExpBookDAL
    {
        public static int AddNewCapitalExpendituresBook(CapitalExpendituresBook CEB)
        {
            try
            {
                using (PropertyCompanyEntities dbcontext = new PropertyCompanyEntities())
                {
                    dbcontext.CapitalExpendituresBooks.Add(CEB);
                    if (dbcontext.SaveChanges() > 0)
                        return 1;

                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static Task<int> AddNewCapitalExpendituresBookAsync(CapitalExpendituresBook CEB)
        {
            return Task.Run(() => AddNewCapitalExpendituresBook(CEB));
        }
    }
}