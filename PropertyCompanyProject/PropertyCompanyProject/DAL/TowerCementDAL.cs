﻿using System;
using System.Threading.Tasks;
using PropertyCompanyProject.Models;

namespace PropertyCompanyProject.DAL
{
    public class TowerCementDAL
    {
        public static int AddTowerCementCost(TowerCementCost nwCement)
        {
            try
            {
                using (PropertyCompanyEntities dbcontext = new PropertyCompanyEntities())
                {
                    dbcontext.TowerCementCosts.Add(nwCement);
                    if (dbcontext.SaveChanges() > 0)
                        return 1;


                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static Task<int> AddTowerCementCostAsync(TowerCementCost nwBook)
        {
            return Task.Run(() => AddTowerCementCost(nwBook));
        }

    }
}