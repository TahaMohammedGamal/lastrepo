﻿using System;
using System.Threading.Tasks;
using PropertyCompanyProject.Models;

namespace PropertyCompanyProject.DAL
{
    public class TowerWorkerFeesDAL
    {
        public static int AddTowerWorkerFee(TowerWorkerFee nwWorkerFee)
        {
            try
            {
                using (PropertyCompanyEntities dbcontext = new PropertyCompanyEntities())
                {
                    dbcontext.TowerWorkerFees.Add(nwWorkerFee);
                    if (dbcontext.SaveChanges() > 0)
                        return 1;


                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static Task<int> AddTowerWorkerFeeAsync(TowerWorkerFee nwBook)
        {
            return Task.Run(() => AddTowerWorkerFee(nwBook));
        }
    }
}