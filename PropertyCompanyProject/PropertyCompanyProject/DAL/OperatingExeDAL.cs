﻿using System;
using System.Linq;
using System.Threading.Tasks;
using PropertyCompanyProject.Models;

namespace PropertyCompanyProject.DAL
{
    public class OperatingExeDAL
    {
        public static int AddNewOperatingExeBook(ExpensesBook nwBook)
        {
            try
            {
                using (PropertyCompanyEntities dbcontext = new PropertyCompanyEntities())
                {
                    dbcontext.ExpensesBooks.Add(nwBook);
                    if (dbcontext.SaveChanges() > 0)
                    {
                        OperatingExpensesBook op = dbcontext.OperatingExpensesBooks.FirstOrDefault();
                        op.TotalExpensesBook += nwBook.Amount;
                        dbcontext.SaveChanges();
                        return 1;
                    }


                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static Task<int> AddNewOperatingExeBookAsync(ExpensesBook nwBook)
        {
            return Task.Run(() => AddNewOperatingExeBook(nwBook));
        }

    }
}