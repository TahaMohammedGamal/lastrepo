﻿using System;
using System.Threading.Tasks;
using PropertyCompanyProject.Models;

namespace PropertyCompanyProject.DAL
{
    public class CommissionsBookDAL
    {
        public static int AddNewCommissionsBook(CommissionsBook commissionsBook)
        {
            try
            {
                using (var dbContext = new PropertyCompanyEntities())
                {
                    dbContext.CommissionsBooks.Add(commissionsBook);
                    var i = dbContext.SaveChanges();
                    return i > 0 ? 1 : 0;
                }
            }
            catch (Exception ex)
            {
                //throw ex;
                return 0;
            }
        }

        public static Task<int> AddNewCommissionsBookAsync(CommissionsBook commissionsBook)
        {
            return Task.Run(() => AddNewCommissionsBook(commissionsBook));
        }
    }
}