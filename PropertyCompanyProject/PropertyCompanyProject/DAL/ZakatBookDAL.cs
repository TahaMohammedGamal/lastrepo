﻿using System;
using System.Threading.Tasks;
using PropertyCompanyProject.Models;

namespace PropertyCompanyProject.DAL
{
    public class ZakatBookDAL
    {
        public static int AddNewZakatBook(ZakatBook ZB)
        {
            try
            {
                using (PropertyCompanyEntities dbcontext = new PropertyCompanyEntities())
                {
                    dbcontext.ZakatBooks.Add(ZB);
                    if (dbcontext.SaveChanges() > 0)
                        return 1;

                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static Task<int> AddNewZakatBookAsync(ZakatBook ZK)
        {
            return Task.Run(() => AddNewZakatBook(ZK));
        }
    }
}