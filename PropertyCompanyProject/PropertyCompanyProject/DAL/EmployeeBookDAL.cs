﻿using PropertyCompanyProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace PropertyCompanyProject.DAL
{
    public class EmployeeBookDAL
    {
        public static int AddNewEmployee(Employee nwEmployee)
        {
            try
            {
                using (PropertyCompanyEntities dbcontext = new PropertyCompanyEntities())
                {
                    dbcontext.Employees.Add(nwEmployee);
                    if (dbcontext.SaveChanges() > 0)
                        return 1;

                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Task<int> AddNewEmployeeAsync(Employee nwEmployee)
        {
            return Task.Run(() => AddNewEmployee(nwEmployee));
        }
    }
}