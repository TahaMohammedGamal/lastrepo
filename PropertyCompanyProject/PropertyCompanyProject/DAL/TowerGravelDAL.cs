﻿using System;
using System.Threading.Tasks;
using PropertyCompanyProject.Models;


namespace PropertyCompanyProject.DAL
{
    public class TowerGravelDAL
    {
        public static int AddTowerGravelCost(TowerGravelCost nwGravel)
        {
            try
            {
                using (PropertyCompanyEntities dbcontext = new PropertyCompanyEntities())
                {
                    dbcontext.TowerGravelCosts.Add(nwGravel);
                    if (dbcontext.SaveChanges() > 0)
                        return 1;


                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static Task<int> AddTowerGravelCostAsync(TowerGravelCost nwBook)
        {
            nwBook.TotalCost = Convert.ToDecimal(nwBook.Quantity) * nwBook.UnitCost;
            return Task.Run(() => AddTowerGravelCost(nwBook));
        }
    }
}