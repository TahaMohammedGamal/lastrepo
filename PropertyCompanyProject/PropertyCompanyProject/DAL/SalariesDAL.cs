﻿using System;
using System.Threading.Tasks;
using PropertyCompanyProject.Models;

namespace PropertyCompanyProject.DAL
{
    public class SalariesDAL
    {
        public static int AddEmployeeSalary(SalariesBook nwSalary)
        {
            try
            {
                using (PropertyCompanyEntities dbcontext = new PropertyCompanyEntities())
                {
                    dbcontext.SalariesBooks.Add(nwSalary);
                    if (dbcontext.SaveChanges() > 0)
                        return 1;


                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Task<int> AddNewSalariesBookAsync(SalariesBook nwBook)
        {
            return Task.Run(() => AddEmployeeSalary(nwBook));
        }
    }
}