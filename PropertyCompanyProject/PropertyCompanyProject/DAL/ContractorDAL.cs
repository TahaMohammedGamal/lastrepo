﻿using System;
using System.Threading.Tasks;
using PropertyCompanyProject.Models;

namespace PropertyCompanyProject.DAL
{
    public class ContractorDAL
    {
        public static int AddContractor(Contractor nwContractor)
        {
            try
            {
                using (PropertyCompanyEntities dbcontext = new PropertyCompanyEntities())
                {
                    dbcontext.Contractors.Add(nwContractor);
                    if (dbcontext.SaveChanges() > 0)
                        return 1;

                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static Task<int> AddContractorAsync(Contractor nwContractor)
        {
            return Task.Run(() => AddContractor(nwContractor));
        }

    }
}