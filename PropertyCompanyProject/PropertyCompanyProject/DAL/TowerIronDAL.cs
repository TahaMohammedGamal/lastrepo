﻿using System;
using System.Threading.Tasks;
using PropertyCompanyProject.Models;

namespace PropertyCompanyProject.DAL
{
    public class TowerIronDAL
    {
        public static int AddTowerIronCost(TowerIronCost nwIron)
        {
            try
            {
                using (PropertyCompanyEntities dbcontext = new PropertyCompanyEntities())
                {
                    dbcontext.TowerIronCosts.Add(nwIron);
                    if (dbcontext.SaveChanges() > 0)
                        return 1;


                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static Task<int> AddTowerIronCostAsync(TowerIronCost nwBook)
        {
            nwBook.TotalCost = Convert.ToDecimal(nwBook.Quantity) * nwBook.UnitCost;
            return Task.Run(() => AddTowerIronCost(nwBook));
        }

    }
}