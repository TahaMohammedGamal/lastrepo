﻿using PropertyCompanyProject.Models;
using System;
using System.Threading.Tasks;

namespace PropertyCompanyProject.DAL
{
    public class SalariesBookDAL
    {
        public static int AddNewSalariesBook(SalariesBook SB)
        {
            try
            {
                using (PropertyCompanyEntities dbcontext = new PropertyCompanyEntities())
                {
                    dbcontext.SalariesBooks.Add(SB);
                    if (dbcontext.SaveChanges() > 0)
                        return 1;

                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static Task<int> AddNewSalariesBookAsync(SalariesBook SB)
        {
            return Task.Run(() => AddNewSalariesBook(SB));
        }
    }
}