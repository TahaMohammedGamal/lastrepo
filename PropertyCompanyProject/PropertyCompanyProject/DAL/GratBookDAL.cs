﻿using System;
using System.Threading.Tasks;
using PropertyCompanyProject.Models;

namespace PropertyCompanyProject.DAL
{
    public class GratBookDAL
    {
        public static int AddNewOperatingGradBook(GratuitiesBook GratBook)
        {
            try
            {
                using (PropertyCompanyEntities dbcontext = new PropertyCompanyEntities())
                {
                    dbcontext.GratuitiesBooks.Add(GratBook);
                    if (dbcontext.SaveChanges() > 0)
                        return 1;

                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static Task<int> AddNewOperatingGradBookAsync(GratuitiesBook GratBook)
        {
            return Task.Run(() => AddNewOperatingGradBook(GratBook));
        }
    }
}