﻿using System.Linq;
using System.Web;
using PropertyCompanyProject.Models;

namespace PropertyCompanyProject
{
    public class User
    {
        public static AspNetUser CurrentUser()
        {
            PropertyCompanyEntities db = new PropertyCompanyEntities();
            string username = HttpContext.Current.Request.Cookies["auth"].Value.Split('|')[0],
                pass = HttpContext.Current.Request.Cookies["auth"].Value.Split('|')[1];
            var user = db.AspNetUsers.Where(u => u.UserName == username && u.PasswordHash == pass).FirstOrDefault();

            return user;
        }
    }
}