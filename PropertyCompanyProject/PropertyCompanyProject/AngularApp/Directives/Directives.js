﻿app.directive('numberOnly', function ($parse) { //to use it type number-Only
    return {
        scope: {
        }, require: "?ngModel",

        link: function (scope, elm, attrs, ngModel) {
            $(elm).bind('keyup', function (e) {
                scope.$apply(function () {
                    
                    //var key;
                    //var keychar;

                    //if (window.event)
                    //    key = window.event.keyCode;
                    //else if (e)
                    //    key = e.which;
                    //else
                    //    return true;

                    //keychar = String.fromCharCode(key);

                    //if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
                    //    return true;
                    //else if ((("0123456789").indexOf(keychar) > -1))
                    //    return true;
                    //else if ($(elm) && (keychar == "."))
                    //    return true;
                    //else
                    //    return false;
                    $(elm).val($(elm).val().replace(/[^0-9.]/g, ''));
                });
            });
        }
    }
});
app.directive('email', function () {//to use it type email
    return {
        restrict: 'A',
        link: function (scope, elm, attrs) {
            elm.bind('blur', function (e) {
                var pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
                if (!(pattern.test($(elm).val())))
                    $(elm).addClass('ng-invalid');
                else
                    $(elm).removeClass('ng-invalid');
            });
        }
    }
});