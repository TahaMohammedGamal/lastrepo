﻿{


    var clientMessages = {
        ServerUnknownError: "Oops, Server error, Please call support !",
        ConnectingServerError: "Error during connecting to server!",
        savedSuccessfully :"تـم إضافة البيانات بناجح",
    };

    app.controller('ExpensesBookController', function ($scope, ExpensesBookServices) {
        $scope.JExpenses = {};
        $scope.disableFlg = false;


        //========================Constructor===========================
        $scope.initBook = function (serverParam) {
            $scope.JExpenses = angular.copy(serverParam.OperObject);
            $scope.NewExpenses = angular.copy(serverParam.OperObject);
            $scope.JExpenses.Date = '';
            $scope.NewExpenses.Date = '';
            $scope.ViewTitle = "دفتر مصروفات نثرية";
            $("#datePicker").datepicker();
        };

        //========================Watches===============================

        //========================Basic Info============================

        $scope.saveData = function () {
            $scope.ShowWaitDialog();
            ExpensesBookServices.saveData($scope.JExpenses).then(function (result) {
                if (result.status == 200) {
                    $scope.HideWaitDialog();
                    var message = $scope.General_FormatString(clientMessages.savedSuccessfully);
                    $scope.ShowAndSetMessageBox("عمـلية نـاجحة", message);
                }
                if (result.status != 200) {
                    $scope.ShowAndSetMessageBox("خـطأ", clientMessages.ConnectingServerError);
                }
                $scope.JExpenses = angular.copy($scope.NewExpenses);
            }, function (error) {
                $scope.ShowAndSetMessageBox("خـطأ", clientMessages.ServerUnknownError);
            })
        };

        //========================Common ===============================
        $scope.ShowWaitDialog = function (waitmsg) {
            waitmsg = typeof waitmsg !== 'undefined' ? waitmsg : "Please wait ...";
            $("#wait-modal-progress-msg").text(waitmsg);
            $("#pleaseWaitDialog").modal();
        };

        $scope.HideWaitDialog = function () {
            $("#pleaseWaitDialog").modal('hide');
        };

        $scope.ShowAndSetMessageBox = function (title, text, isError, CallBack) {
            if (typeof isError == 'undefined')
                isError = false;

            $("#alertModal_OkButton").unbind('click');

            document.getElementById("alertTitle").innerHTML = title;
            document.getElementById("messageText").innerHTML = text;

            if (isError) {
                $("#alertTitle").addClass("text-danger");
                $("#messageText").addClass("text-danger");
                $("#alertModal_OkButton").removeClass("btn-primary").addClass("btn-danger");
            }
            else {
                $("#alertTitle").removeClass("text-danger");
                $("#messageText").removeClass("text-danger");
                $("#alertModal_OkButton").removeClass("btn-danger").addClass("btn-primary");
            }

            if (typeof CallBack !== 'undefined' && CallBack != null)
                $("#alertModal_OkButton").on("click", function () {
                    CallBack();
                });
            $('#alertModal').modal('show');
        };

        $scope.General_FormatString = function (message, args) {
            if (message == undefined || message == null)
                return null;
            if (args == undefined || args == null)
                return message;
            if (args.length == 0)
                return message;
            for (i = 0; i < args.length ; i++)
                message = message.replace("{" + i + "}", args[i])
            return message;
        }


    })
}