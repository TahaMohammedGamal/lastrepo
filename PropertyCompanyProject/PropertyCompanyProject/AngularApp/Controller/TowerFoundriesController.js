﻿
{


    var clientMessages = {
        ServerUnknownError: "يبدو أن هناك مشكله ما , من فضلك تواصل مع الدعم الفنى !!",
        ConnectingServerError: " هناك مشكله ما فى الاتصال بالانترنت !!",
        savedSuccessfully: "تـم إضافة البيانات بنجاح",
    };

    app.controller('TowerFoundriesController', function ($scope, TowerFoundriesServices) {
        $scope.JTowerFoundries = {};
        $scope.disableFlg = false;


        //========================Constructor===========================
        $scope.initBook = function (serverParam) {
            $scope.JTowerFoundries = angular.copy(serverParam.Foundries);
            $scope.JTowerFoundries.Date = "";
            $scope.NewTowerFoundries = angular.copy(serverParam.Foundries);
            $scope.NewTowerFoundries.Date = "";
            $scope.Towers = angular.copy(serverParam.Towers);
            $scope.Contractors = angular.copy(serverParam.Contractors);
            $("#datePicker").datepicker();
            $scope.ViewTitle = "إضـافة بيـانـات دفـتر السباكة";

        };

        //========================Watches===============================

        //========================Basic Info============================

        $scope.saveData = function () {
            $scope.ShowWaitDialog();
            TowerFoundriesServices.saveData($scope.JTowerFoundries).then(function (result) {
                if (result.status == 200) {
                    $scope.HideWaitDialog();

                    var message = $scope.General_FormatString(clientMessages.savedSuccessfully);
                    $scope.ShowAndSetMessageBox("عمـلية نـاجحة", message);
                }
                if (result.status != 200) {
                    //swal("خطأ ...", "يبدو أن هناك مشكله ما , من فضلك تواصل مع الدعم الفنى !!", "error");
                    $scope.ShowAndSetMessageBox("خـطأ", clientMessages.ConnectingServerError);
                }
                $scope.JTowerFoundries = angular.copy($scope.NewTowerFoundries);
            }, function (error) {
                $scope.ShowAndSetMessageBox("خـطأ", clientMessages.ServerUnknownError);
            })
        };

        //========================Common ===============================
        $scope.ShowWaitDialog = function (waitmsg) {
            waitmsg = typeof waitmsg !== 'undefined' ? waitmsg : "Please wait ...";
            $("#wait-modal-progress-msg").text(waitmsg);
            $("#pleaseWaitDialog").modal();
        };

        $scope.HideWaitDialog = function () {
            $("#pleaseWaitDialog").modal('hide');
        };

        $scope.ShowAndSetMessageBox = function (title, text, isError, CallBack) {
            if (typeof isError == 'undefined')
                isError = false;

            $("#alertModal_OkButton").unbind('click');

            document.getElementById("alertTitle").innerHTML = title;
            document.getElementById("messageText").innerHTML = text;

            if (isError) {
                $("#alertTitle").addClass("text-danger");
                $("#messageText").addClass("text-danger");
                $("#alertModal_OkButton").removeClass("btn-primary").addClass("btn-danger");
            }
            else {
                $("#alertTitle").removeClass("text-danger");
                $("#messageText").removeClass("text-danger");
                $("#alertModal_OkButton").removeClass("btn-danger").addClass("btn-primary");
            }

            if (typeof CallBack !== 'undefined' && CallBack != null)
                $("#alertModal_OkButton").on("click", function () {
                    CallBack();
                });
            $('#alertModal').modal('show');
        };

        $scope.General_FormatString = function (message, args) {
            if (message == undefined || message == null)
                return null;
            if (args == undefined || args == null)
                return message;
            if (args.length == 0)
                return message;
            for (i = 0; i < args.length ; i++)
                message = message.replace("{" + i + "}", args[i])
            return message;
        }

    })
}