﻿{


    var clientMessages = {
        ServerUnknownError: "Oops, Server error, Please call support !",
        ConnectingServerError: "Error during connecting to server!",
        savedSuccessfully: "تـم إضافة البيانات بناجح",
    };

    app.controller('TowerDetailsController', function ($scope, TowerDetailsServices) {
        $scope.JTower = {};
        $scope.disableFlg = false;


        //========================Constructor===========================
        $scope.initBook = function (serverParam) {
            $scope.JTower = angular.copy(serverParam.TowerObject);
            $scope.NewTower = angular.copy(serverParam.TowerObject);
            $scope.ViewTitle = "إضـافة بـرج جـديد";
        };

        //========================Watches===============================

        //========================Basic Info============================

        $scope.saveData = function () {
            //$scope.boollvar = true;
            $scope.ShowWaitDialog();
            //ShowProgressServices.ShowProgress($scope.boollvar);
            TowerDetailsServices.saveData($scope.JTower).then(function (result) {
                if (result.status == 200) {
                    $scope.HideWaitDialog();
                    
                    //var message = General_FormatString(clientMessages.savedSuccessfully);
                    //ShowAndSetMessageBox("Success", message);
                }
                if (result.status != 200) {
                    swal("خطأ ...", "يبدو أن هناك مشكله ما , من فضلك تواصل مع الدعم الفنى !!", "error");
                    //alert("يبدو أن هناك مشكله ما , من فضلك تواصل مع الدعم الفنى !!")
                }
                $scope.JTower = angular.copy($scope.NewTower);
            }, function (error) {
                swal("خطأ ...", "يبدو أن هناك مشكله ما , من فضلك تواصل مع الدعم الفنى !!", "error");
                //alert("يبدو أن هناك مشكله ما , من فضلك تواصل مع الدعم الفنى !!");
            })
        };

        //========================Common ===============================
        $scope.ShowWaitDialog = function (waitmsg) {
            waitmsg = typeof waitmsg !== 'undefined' ? waitmsg : "Please wait ...";
            $("#wait-modal-progress-msg").text(waitmsg);
            $("#pleaseWaitDialog").modal();
        };

        $scope.HideWaitDialog = function () {
            $("#pleaseWaitDialog").modal('hide');
        };

    })
}