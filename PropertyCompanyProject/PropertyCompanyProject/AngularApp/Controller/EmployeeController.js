﻿{
                var clientMessages = {
                    ServerUnknownError: "يبدو أن هناك مشكله ما , من فضلك تواصل مع الدعم الفنى !!",
                    ConnectingServerError: " هناك مشكله ما فى الاتصال بالانترنت !!",
                    savedSuccessfully: "تـم إضافة البيانات بنجاح"
                };

                app.controller('EmployeesBookController', function ($scope, EmployeeServices) {
                    $scope.emp = {};
                    $scope.disableFlg = false;
                

                //========================Constructor===========================
                $scope.initBook = function (serverParam) {
                    $scope.emp = angular.copy(serverParam.OperObject);
                    $scope.Newemp = angular.copy(serverParam.OperObject);
                    $scope.ViewTitle = "اضافة بيانات الموظف";
                };

                //========================Basic Info============================
                $scope.saveData = function () {
                    $scope.ShowWaitDialog();
                    EmployeeServices.saveData($scope.emp).then(function (result) {
                        $scope.HideWaitDialog();
                        if (result.status == 200) {
                            var message = $scope.General_FormatString(clientMessages.savedSuccessfully);
                            $scope.ShowAndSetMessageBox("عمـلية نـاجحة", message);
                        }
                        if (result.status != 200) {
                            $scope.ShowAndSetMessageBox("خـطأ", clientMessages.ConnectingServerError);
                        }
                        $scope.emp = angular.copy($scope.Newemp);
                    }, function (error) {
                        $scope.ShowAndSetMessageBox("خـطأ", clientMessages.ServerUnknownError);
                    });
                };

                //========================Common ===============================
                $scope.ShowWaitDialog = function(waitmsg) {
                    waitmsg = typeof waitmsg !== 'undefined' ? waitmsg : "Please wait ...";
                    $("#wait-modal-progress-msg").text(waitmsg);
                    $("#pleaseWaitDialog").modal();
                };

                $scope.HideWaitDialog = function() {
                    $("#pleaseWaitDialog").modal('hide');
                };

                $scope.ShowAndSetMessageBox = function(title, text, isError, CallBack) {
                    if (typeof isError == 'undefined')
                        isError = false;

                    $("#alertModal_OkButton").unbind('click');

                    document.getElementById("alertTitle").innerHTML = title;
                    document.getElementById("messageText").innerHTML = text;

                    if (isError) {
                        $("#alertTitle").addClass("text-danger");
                        $("#messageText").addClass("text-danger");
                        $("#alertModal_OkButton").removeClass("btn-primary").addClass("btn-danger");
                    } else {
                        $("#alertTitle").removeClass("text-danger");
                        $("#messageText").removeClass("text-danger");
                        $("#alertModal_OkButton").removeClass("btn-danger").addClass("btn-primary");
                    }

                    if (typeof CallBack !== 'undefined' && CallBack != null)
                        $("#alertModal_OkButton").on("click", function() {
                            CallBack();
                        });
                    $('#alertModal').modal('show');
                };

                $scope.General_FormatString =
                    function (message, args) {
                    if (message == undefined || message == null)
                        return null;
                    if (args == undefined || args == null)
                        return message;
                    if (args.length == 0)
                        return message;
                        for (i = 0; i < args.length; i++)
                            message = message.replace("{" + i + "}", args[i]);
                    return message;
                    }
                });
}