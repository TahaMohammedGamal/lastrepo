﻿{
    var clientMessages = {
        ServerUnknownError: "يبدو أن هناك مشكله ما , من فضلك تواصل مع الدعم الفنى !!",
        ConnectingServerError: " هناك مشكله ما فى الاتصال بالانترنت !!",
        savedSuccessfully: "تـم إضافة البيانات بنجاح",
    };

    app.controller('ZakatBookController', function($scope, ZakatBookServices) {
        $scope.ZakatB = {};
        $scope.disableFlg = false;


        //========================Constructor===========================
        $scope.initBook = function(serverParam) {
            $scope.ZakatB = angular.copy(serverParam.OperObject);
            $scope.NewZakatB = angular.copy(serverParam.OperObject);
            $scope.ZakatB.Date = '';
            $scope.NewZakatB.Date = '';
            $scope.ViewTitle = "دفتر الزكاة والصدقه";
            $("#datePickerZakat").datepicker();
        };

        //========================Watches===============================

        //========================Basic Info============================
        $scope.saveData = function() {
            $scope.ShowWaitDialog();
            ZakatBookServices.saveData($scope.ZakatB).then(function(result) {
                $scope.HideWaitDialog();
                if (result.status == 200) {
                    var message = $scope.General_FormatString(clientMessages.savedSuccessfully);
                    $scope.ShowAndSetMessageBox("عمـلية نـاجحة", message);
                }
                if (result.status != 200) {
                    $scope.ShowAndSetMessageBox("خـطأ", clientMessages.ConnectingServerError);
                }
                $scope.ZakatB = angular.copy($scope.NewZakatB);
            }, function(error) {
                $scope.ShowAndSetMessageBox("خـطأ", clientMessages.ServerUnknownError);
            });
        };

        //========================Common ===============================
        $scope.ShowWaitDialog = function(waitmsg) {
            waitmsg = typeof waitmsg !== 'undefined' ? waitmsg : "Please wait ...";
            $("#wait-modal-progress-msg").text(waitmsg);
            $("#pleaseWaitDialog").modal();
        };

        $scope.HideWaitDialog = function() {
            $("#pleaseWaitDialog").modal('hide');
        };

        $scope.ShowAndSetMessageBox = function(title, text, isError, CallBack) {
            if (typeof isError == 'undefined')
                isError = false;

            $("#alertModal_OkButton").unbind('click');

            document.getElementById("alertTitle").innerHTML = title;
            document.getElementById("messageText").innerHTML = text;

            if (isError) {
                $("#alertTitle").addClass("text-danger");
                $("#messageText").addClass("text-danger");
                $("#alertModal_OkButton").removeClass("btn-primary").addClass("btn-danger");
            } else {
                $("#alertTitle").removeClass("text-danger");
                $("#messageText").removeClass("text-danger");
                $("#alertModal_OkButton").removeClass("btn-danger").addClass("btn-primary");
            }

            if (typeof CallBack !== 'undefined' && CallBack != null)
                $("#alertModal_OkButton").on("click", function() {
                    CallBack();
                });
            $('#alertModal').modal('show');
        };

        $scope.General_FormatString = function(message, args) {
            if (message == undefined || message == null)
                return null;
            if (args == undefined || args == null)
                return message;
            if (args.length == 0)
                return message;
            for (i = 0; i < args.length; i++)
                message = message.replace("{" + i + "}", args[i])
            return message;
        }
    })
}
