﻿var clientMessages = {
    ServerUnknownError: "يبدو أن هناك مشكله ما , من فضلك تواصل مع الدعم الفنى !!",
    ConnectingServerError: " هناك مشكله ما فى الاتصال بالانترنت !!",
    SavedSuccessfully: "تـم إضافة البيانات بنجاح"
};

app.controller("CommissionsController", function ($scope, commissionServices) {
    $scope.Commission = {};

    //========================Constructor===========================
    $scope.initCommissionsController = function(serverParam) {
        if (serverParam !== undefined && serverParam !== null)
            $scope.Commission = angular.copy(serverParam.Commission);
        $scope.Commission.Date = "";
        $scope.NewCommission = angular.copy($scope.Commission);
        $("#commissionDate").datepicker();
        $scope.ViewTitle = "إضـافة بيـانـات دفـتر العمولات";
    };

    //========================Basic Info============================
    $scope.saveData = function() {
        $scope.ShowWaitDialog();
        commissionServices.saveData($scope.Commission).then(function(result) {
            
            if (result.status === 200) {
                $scope.HideWaitDialog();
                var message = $scope.General_FormatString(clientMessages.SavedSuccessfully);
                $scope.ShowAndSetMessageBox("عمـلية نـاجحة", message);
            }
            if (result.status !== 200) {
                $scope.ShowAndSetMessageBox("خـطأ", clientMessages.ConnectingServerError);
            }
            $scope.Commission = angular.copy($scope.NewCommission);
            $scope.basicForm.$setPristine();
        }, function(error) {
            $scope.ShowAndSetMessageBox("خـطأ", clientMessages.ServerUnknownError);
        });
    };

    //========================Common ===============================
    $scope.ShowWaitDialog = function (waitmsg) {
        waitmsg = typeof waitmsg !== 'undefined' ? waitmsg : "Please wait ...";
        $("#wait-modal-progress-msg").text(waitmsg);
        $("#pleaseWaitDialog").modal();
    };

    $scope.HideWaitDialog = function () {
        $("#pleaseWaitDialog").modal('hide');
    };

    $scope.ShowAndSetMessageBox = function (title, text, isError, CallBack) {
        if (typeof isError == 'undefined')
            isError = false;

        $("#alertModal_OkButton").unbind('click');

        document.getElementById("alertTitle").innerHTML = title;
        document.getElementById("messageText").innerHTML = text;

        if (isError) {
            $("#alertTitle").addClass("text-danger");
            $("#messageText").addClass("text-danger");
            $("#alertModal_OkButton").removeClass("btn-primary").addClass("btn-danger");
        }
        else {
            $("#alertTitle").removeClass("text-danger");
            $("#messageText").removeClass("text-danger");
            $("#alertModal_OkButton").removeClass("btn-danger").addClass("btn-primary");
        }

        if (typeof CallBack !== 'undefined' && CallBack != null)
            $("#alertModal_OkButton").on("click", function () {
                CallBack();
            });
        $('#alertModal').modal('show');
    };

    $scope.General_FormatString = function (message, args) {
        if (message == undefined || message == null)
            return null;
        if (args == undefined || args == null)
            return message;
        if (args.length == 0)
            return message;
        for (i = 0; i < args.length ; i++)
            message = message.replace("{" + i + "}", args[i])
        return message;
    }

});