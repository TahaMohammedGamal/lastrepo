﻿app.factory('TowerElectricityServices', function ($http) {

    var ElectricityServices = {};

    ElectricityServices.saveData = function (JElectricity) {
        return $http.post('/TowerElectricity/SaveData', JElectricity, { headers: { 'Content-Type': 'application/json' } });
    };

    return ElectricityServices;
});
