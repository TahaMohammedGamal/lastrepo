﻿app.factory('TowerFoundriesServices', function ($http) {

    var FoundriesServices = {};

    FoundriesServices.saveData = function (JCement) {
        return $http.post('/TowerFoundries/SaveData', JCement, { headers: { 'Content-Type': 'application/json' } });
    };

    return FoundriesServices;
});
