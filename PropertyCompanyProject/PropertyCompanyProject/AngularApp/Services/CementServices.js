﻿
app.factory('CementServices', function ($http) {

    var CementServices = {};

    CementServices.saveData = function (JCement) {
        return $http.post('/TowerCement/SaveData', JCement, { headers: { 'Content-Type': 'application/json' } });
    };

    return CementServices;
});
