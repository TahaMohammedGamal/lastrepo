﻿app.factory('TowerSandServices', function ($http) {

    var SandServices = {};

    SandServices.saveData = function (JSand) {
        return $http.post('/TowerSand/SaveData', JSand, { headers: { 'Content-Type': 'application/json' } });
    };

    return SandServices;
});
