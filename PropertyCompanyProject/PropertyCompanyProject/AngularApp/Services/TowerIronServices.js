﻿
app.factory('TowerIronServices', function ($http) {

    var TowerIronServices = {};

    TowerIronServices.saveData = function (JIron) {
        return $http.post('/TowerIron/SaveData', JIron, { headers: { 'Content-Type': 'application/json' } });
    };

    return TowerIronServices;
});

