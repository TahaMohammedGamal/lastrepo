﻿
app.factory('ContractorsServices', function ($http) {

    var ContractorsServices = {};

    ContractorsServices.saveData = function (JContractor) {
        return $http.post('/Contractor/SaveData', JContractor, { headers: { 'Content-Type': 'application/json' } });
    };

    return ContractorsServices;
});