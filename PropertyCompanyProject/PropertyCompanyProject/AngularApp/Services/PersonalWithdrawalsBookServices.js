﻿
app.factory('PersonalWithdrawalsBookServices', function ($http) {

    var PersonalWithdrawalsBookServices = {};

    PersonalWithdrawalsBookServices.saveData = function (Personaldrawalsbook) {
        return $http.post('/PersonalWithdrawalsBook/SaveData', Personaldrawalsbook, { headers: { 'Content-Type': 'application/json' } });
    };

    return PersonalWithdrawalsBookServices;
});