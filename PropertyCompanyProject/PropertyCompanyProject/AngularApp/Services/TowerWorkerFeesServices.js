﻿app.factory('TowerWorkerFeesServices', function ($http) {

    var WorkerFeesServices = {};

    WorkerFeesServices.saveData = function (JWorkerFees) {
        return $http.post('/TowerWorkerFees/SaveData', JWorkerFees, { headers: { 'Content-Type': 'application/json' } });
    };

    return WorkerFeesServices;
});
