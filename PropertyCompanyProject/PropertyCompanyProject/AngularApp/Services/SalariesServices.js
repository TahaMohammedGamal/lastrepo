﻿
app.factory('SalariesServices', function ($http) {

    var SalariesServices = {};

    SalariesServices.saveData = function (JSalariesBook) {
        return $http.post('/SalariesBook/SaveData', JSalariesBook, { headers: { 'Content-Type': 'application/json' } });
    };

    return SalariesServices;
});