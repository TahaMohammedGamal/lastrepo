﻿
app.factory('ExpensesBookServices', function ($http) {
    
    var ExpensesBookServices = {};

    ExpensesBookServices.saveData = function (JExpenses) {
        return $http.post('/OperatingExpensesBook/SaveData',JExpenses, { headers: { 'Content-Type': 'application/json' } });
    };
    
    return ExpensesBookServices;
});