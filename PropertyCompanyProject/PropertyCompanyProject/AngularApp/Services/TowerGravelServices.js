﻿app.factory('TowerGravelServices', function ($http) {

    var GravelServices = {};

    GravelServices.saveData = function (JGravel) {
        return $http.post('/TowerGravel/SaveData', JGravel, { headers: { 'Content-Type': 'application/json' } });
    };

    return GravelServices;
});
