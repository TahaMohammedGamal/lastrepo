﻿
app.factory('ZakatBookServices', function ($http) {

    var ZakatBookServices = {};

    ZakatBookServices.saveData = function (JZakatBook) {
        return $http.post('/ZakatBook/SaveData', JZakatBook, { headers: { 'Content-Type': 'application/json' } });
    };

    return ZakatBookServices;
});