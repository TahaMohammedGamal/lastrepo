﻿app.factory("commissionServices", function($http) {
    return {
        saveData: function(jCommission) {
            return $http.post("/CommissionsBook/SaveData", jCommission, { headers: { "Content-Type": "application/json" } });
        }
    }
});