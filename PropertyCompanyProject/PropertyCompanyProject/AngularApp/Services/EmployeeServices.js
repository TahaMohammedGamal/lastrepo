﻿
app.factory('EmployeeServices', function ($http) {

    var EmployeeServices = {};

    EmployeeServices.saveData = function (emp) {
        return $http.post('/EmployeesBook/SaveData', emp, { headers: { 'Content-Type': 'application/json' } });
    };

    return EmployeeServices;
});