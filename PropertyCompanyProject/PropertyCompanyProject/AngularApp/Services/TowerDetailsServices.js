﻿
app.factory('TowerDetailsServices', function ($http) {

    var TowerDetailsServices = {};

    TowerDetailsServices.saveData = function (JExpenses) {
        return $http.post('/Tower/SaveData', JExpenses, { headers: { 'Content-Type': 'application/json' } });
    };

    return TowerDetailsServices;
});