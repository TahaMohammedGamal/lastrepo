﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace PropertyCompanyProject.HelperClasses
{
    public class Helper
    {
        public static string ConvertDataToJson(Object obj)
        {
            try
            {
                return JsonConvert.SerializeObject(obj, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public class ServerParam
    {
        public string Key { set; get; }
        public object Value { set; get; }

        public ServerParam()
        {

        }

        public ServerParam(string key, object value)
        {
            Key = key;
            Value = value;
        }

    }

    public class ClientBag : Dictionary<string, object>
    {
        public ClientBag()
        {

        }

        public ClientBag(string key, object val)
        {
            Add(key, val);
        }

        public ClientBag(params ServerParam[] parameters)
        {
            foreach (ServerParam parameter in parameters)
                Add(parameter.Key, parameter.Value);
        }

        public static ClientBag operator +(ClientBag obj, ServerParam parameter)
        {
            obj.Add(parameter.Key, parameter.Value);
            return obj;
        }

        public static ClientBag operator -(ClientBag obj, string key)
        {
            obj.Remove(key);
            return obj;
        }
    }
}